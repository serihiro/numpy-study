import numpy as np
import pickle

array = np.random.rand(200000, 1000)
print(array.shape)

with open('numpy.pickle', mode='wb') as f:
    pickle.dump(array, f, protocol=3)

with open('numpy.pickle', mode='rb') as f:
    loaded_array = pickle.load(f)

print(loaded_array.shape)

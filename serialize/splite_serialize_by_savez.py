import numpy as np

array = np.random.rand(201, 100)
splited_array = np.array_split(array, 4)

for i in range(len(splited_array)):
    target = splited_array[i]
    print(target.shape)
    np.savez_compressed(f'array{i}.npz', array=target)

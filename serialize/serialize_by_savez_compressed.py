import numpy as np

array = np.random.rand(200000, 10000)
print(array.shape)
np.savez_compressed('array.npz', array=array)
print(np.load('array.npz', 'r')['array'].shape)
